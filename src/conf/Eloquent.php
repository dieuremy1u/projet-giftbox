<?php
namespace conf;
/**
 * Created by PhpStorm.
 * User: JEDETESTEMICROSOFT
 * Date: 06/12/2016
 * Time: 11:40
 */
use Illuminate\Database\Capsule\Manager as DB;

class Eloquent
{
        public static function init($file){
            $db = new DB();
            $db->addConnection(parse_ini_file($file));
            $db->setAsGlobal();
            $db->bootEloquent();
    }
}