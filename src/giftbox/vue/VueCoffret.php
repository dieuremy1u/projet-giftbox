<?php
/**
 * Created by PhpStorm.
 * User: Mathilde
 * Date: 14/12/2016
 * Time: 16:51
 */

namespace giftbox\vue;



use giftbox\models\Coffret;
use giftbox\models\Prestation;
use RandomLib\Factory;
use Slim\Slim;

class VueCoffret
{

    private $tableau_prestation;
    private $validation;
    private $contient;

    public function __construct($prest=null,$coffre=null, $contient=null){
        $this->tableau_prestation=$prest;
        $this->validation=$coffre;
        $this->contient=$contient;
    }

    private function afficherListePrestations() {
        $uri = \Slim\Slim::getInstance()->request->getRootUri();
        $nbpresta = 0;
        $cout = 0;
        if (isset($_SESSION['prestations_coffret'])) {
            foreach (array_keys($_SESSION['prestations_coffret']) as $id)
            {
                $nbpresta += $_SESSION['prestations_coffret'][$id];
                $cout += $_SESSION['prestations_coffret'][$id]*$this->tableau_prestation[$id]->prix;
            }
        }
        $string = "
            <div class=\"titre-panier\">
                <h3>Votre coffret en construction</h3>
                <br>Vous avez $nbpresta prestations dans votre coffret
                <br>Montant total : $cout €
            </div>
        ";

        $slim=\Slim\Slim::getInstance();
            foreach($this->tableau_prestation as $prestation) {
                $string .= "
                    <div class=\"col-sm-12 col-md-12\">
                        <div class=\"thumbnail\">
                            <img src=$uri/web/img/" . $prestation->img . " alt='$prestation->nom' id='img-catalogue'>
                            <div class='desc-detaillee'> 
                                <h3>" . $prestation->nom . "</h3>
                                Catégorie : " . $prestation->getCategorie()->nom . "
                                <br>Prix : " . $prestation->prix . " €
                                <br>
                                <br>" . $prestation->descr . "
                            </div>
                            <div class='desc-qte'> 
                                Quantité :" .$_SESSION['prestations_coffret'][$prestation->id]. "
                            </div>
                            <a href=".$slim->urlFor('coffret/supp',['id'=> $prestation->id]).">
                                <div class='desc-bouton'>
                                    <h5>Supprimer</h5>
                                </div>
                            </a>
                        </div>
                    </div>
                ";
            }
        $slim = \Slim\Slim::getInstance();

        $string .= "
            <a href=".$slim->urlFor('coffret/valider').">
                <div class='desc-bouton'>
                    <h3>Valider le coffret</h3>
                </div>  
            </a>
        ";

        return $string;
    }

    public function afficherErreurValidation(){
        $string="<div class=\"titre-panier\">
                Vous ne pouvez pas valider un coffret contenant moins de
                <BR> 2 Prestations, de 2 Catégories différentes !
            </div>";

        return $string;
    }

    public function afficherFormulaire(){
        $slim = \Slim\Slim::getInstance();
        $string="
            <div class=\"titre-panier\"> 
                <h3>Veuillez saisir vos informations personnelles</h3>
                <BR>
                <form method=\"post\" action=\"recap\">
                    <p>
                    <div class=\"titre-panier\">
                        Nom :    <input type=\"text\" name=\"nom\" /><BR>
                        Prénom : <input type=\"text\" name=\"prenom\" /><BR>
                        Mail :   <input type=\"text\" name=\"mail\" /><BR>
                    </div>
                    <BR>
                    <div class=\"titre-panier\">
                        Message transmis avec le coffret : 
                        <BR><textarea name=\"message\" rows=\"8\" cols=\"45\">J'espère que ce cadeau te plaira !</textarea><BR>
                            Voulez vous un mot de passe pour le coffret ? <BR> 
                        <input type=\"radio\" name=\"mdp\" value=\"oui\" id=\"oui\"  />
                        <label for=\"oui\">Oui</label>
                        <input type=\"radio\" name=\"mdp\" value=\"non\" id=\"non\" checked=\"checked\"/>
                        <label for=\"non\">Non</label>
                        <BR> Si oui veuillez le renseigner ici : <input type=\"password\" name=\"motdepasse\" /> 
                    <div/>
                    <BR>
                    <div class=\"titre-panier\">
                        Quel mode de paiement ?
                        <input type=\"radio\" name=\"paiement\" value=\"classique\" id=\"classique\" checked=\"checked\" />
                        <label for=\"classique\">Classique</label>
                        <input type=\"radio\" name=\"paiement\" value=\"cagnotte\" id=\"cagnote\" />
                        <label for=\"cagnote\">Cagnotte</label>
                    </div>
                    <div class=\"titre-panier\">
                        <input type=\"hidden\" name=\"_METHOD\" value=\"PUT\"/>
                        <input type=\"submit\" value=\"Valider\" />
                    <div/>
                    </p>
                </form>
            </div>
        ";
        return $string;
    }

    public function afficherRecapitulatif(){
        $uri = \Slim\Slim::getInstance()->request->getRootUri();
        $slim = \Slim\Slim::getInstance();
        $content="
            <div class=\"titre-panier\"> 
                <h3>Récapitulatif de votre coffret</h3>
                <BR>Nom : ".$this->validation['nom'].'
                <BR> Prenom : '.$this->validation['prenom'].'
                <BR> Mail : '.$this->validation['mail'].'
                <BR> Votre message : '.$this->validation['message'].'
                <BR> Votre type de Paiement : "'.$this->validation['paiement']."\"
                <BR>
            </div>
        ";
        $nbpresta = 0;
        $cout = 0;
        if (isset($_SESSION['prestations_coffret'])) {
            foreach (array_keys($_SESSION['prestations_coffret']) as $id)
            {
                $nbpresta += $_SESSION['prestations_coffret'][$id];
                $cout += $_SESSION['prestations_coffret'][$id]*$this->tableau_prestation[$id]->prix;
            }
        }
        $_SESSION['cout']=$cout;
        $content.= "
            <div class=\"titre-panier\">
                Vous avez $nbpresta prestations dans votre coffret
                <br>Montant total : $cout €
            </div>
        ";

        foreach($this->tableau_prestation as $prestation) {
            $content.= "
                    <div class=\"col-sm-12 col-md-12\">
                        <div class=\"thumbnail\">
                            <img src=$uri/web/img/" . $prestation->img . " alt='$prestation->nom' id='img-catalogue'>
                            <div class='desc-detaillee'> 
                                <h3>" . $prestation->nom . "</h3>
                                Catégorie : " . $prestation->getCategorie()->nom . "
                                <br>Prix : " . $prestation->prix . " €
                                <br>
                                <br>" . $prestation->descr . "
                            </div>
                            <div class='desc-qte'> 
                                Quantité :" .$_SESSION['prestations_coffret'][$prestation->id]. "
                            </div>
                        </div>
                    </div>
                ";
        }

        $content.="
            <div class='bouton-valider'>
                <a href=".$slim->urlFor('coffret/enregistrer').">
                    <div class='desc-bouton'>
                        <h3>Valider le coffret</h3>
                    </div>  
                </a>
            </div>";

        return $content;
    }

    public function afficherPageGestionCagnotte()
    {
        $slim=Slim::getInstance();
        $uri = \Slim\Slim::getInstance()->request->getRootUri();
        $string = "
            <div class=\"titre-panier\"> 
                <h3>L'état de votre coffret numéro ".$this->tableau_prestation['id']."</h3>
                <br>Votre url de gestion :
                <br><a href='http://".$_SERVER['HTTP_HOST']."$uri/coffret/gestion?token=".$this->tableau_prestation['token_gestion']."'> http://".$_SERVER['HTTP_HOST']."$uri/coffret/gestion?token=".$this->tableau_prestation['token_gestion']." </a>
                <br>Votre url de cagnotte :
                <br><a href='http://".$_SERVER['HTTP_HOST']."$uri/coffret/cagnote?token=".$this->tableau_prestation['token_cagnotte']."'> http://".$_SERVER['HTTP_HOST']."$uri/coffret/gestion?token=".$this->tableau_prestation['token_cagnotte']." </a>
                <br>
                <br>Le montant à payer est de : ".$this->tableau_prestation->prix." €
                <br>La somme d'argent rassemblée par la cagnotte est de : ".$this->tableau_prestation->total_paye." €
                <br>
                <br><h3>Il reste  ".($this->tableau_prestation->prix - $this->tableau_prestation->total_paye)." € à payer</h3>
                <br>Choisissez la somme de votre contribution à la cagnotte :
                <br>
                <form method=\"post\" action=\"$uri/coffret/gestion2?token=".$this->tableau_prestation['token_gestion']."\">
                    <input type='number' name='ajout' step='1' value='0' min='0' max='".($this->tableau_prestation->prix - $this->tableau_prestation->total_paye)."'/>
                    <input type=\"hidden\" name=\"_METHOD\" value=\"PUT\"/>
                    <input type=\"submit\" value=\"Valider\" />
                </form>
                <br>Ce que contient votre commande :
                <br>
            ";

        foreach($this->validation as $prestation) {
            $string .= "
                <div class='col-sm-12 col-md-12'>
                    <div class='thumbnail'>
                        <img src=$uri/web/img/" . $prestation->img . " alt='$prestation->nom' id='img-catalogue'>
                        <div class='desc-detaillee'> 
                            <h3>" . $prestation->nom . "</h3>                             
                            Catégorie : " . $prestation->getCategorie()->nom . "
                            <br>Prix : " . $prestation->prix . " €
                            <br>
                            <br>" . $prestation->descr . "
                        </div>
                        ";

            $contient = null;
            foreach ($this->contient as $c) {
                if ($c->idPrestation == $prestation->id) {
                    $contient = $c;
                }
            }
            $quantite = $contient->quantite;

            $string .= "
                         <div class='desc-qte'> 
                             Quantité :" . $quantite . "
                         </div>
                     </div>
                 </div>
            ";
        }

        return $string;
    }


    public function afficherPageCagnotte()
    {
        $slim=Slim::getInstance();
        $uri = \Slim\Slim::getInstance()->request->getRootUri();
        $string = "
            <div class=\"titre-panier\"> 
                <h3>Participation au coffret numéro ".$this->tableau_prestation['id']."</h3>
                <br>
                <br>Le montant à payer est de : ".$this->tableau_prestation->prix." €
                <br>La somme d'argent rassemblée par la cagnotte est de : ".$this->tableau_prestation->total_paye." €
                <br>
                <br><h3>Il reste  ".($this->tableau_prestation->prix - $this->tableau_prestation->total_paye)." € à payer</h3>
                <br>Choisissez la somme de votre contribution à la cagnotte :
                <br>
                <form method=\"post\" action=\"$uri/coffret/cagnote2?token=".$this->tableau_prestation['token_cagnotte']."\">
                    <input type='number' name='ajout' step='1' value='0' min='0' max='".($this->tableau_prestation->prix - $this->tableau_prestation->total_paye)."'/>
                    <input type=\"hidden\" name=\"_METHOD\" value=\"PUT\"/>
                    <input type=\"submit\" value=\"Valider\" />
                </form>
                <br>Ce que contient la commande :
                <br>
            ";

        foreach($this->validation as $prestation) {
            $string .= "
                <div class='col-sm-12 col-md-12'>
                    <div class='thumbnail'>
                        <img src=$uri/web/img/" . $prestation->img . " alt='$prestation->nom' id='img-catalogue'>
                        <div class='desc-detaillee'> 
                            <h3>" . $prestation->nom . "</h3>                             
                            Catégorie : " . $prestation->getCategorie()->nom . "
                            <br>Prix : " . $prestation->prix . " €
                            <br>
                            <br>" . $prestation->descr . "
                        </div>
                        ";

            $contient = null;
            foreach ($this->contient as $c) {
                if ($c->idPrestation == $prestation->id) {
                    $contient = $c;
                }
            }
            $quantite = $contient->quantite;

            $string .= "
                         <div class='desc-qte'> 
                             Quantité :" . $quantite . "
                         </div>
                     </div>
                 </div>
            ";
        }

        return $string;
    }


    public function afficherPageGestion(){

        $tranmis='';
        switch($this->tableau_prestation['transmis']){
            case 0 :
                $tranmis='non';
                break;
            case 1 :
                $tranmis='oui';
                break;

        }

        $ouvert='';
        switch ($this->tableau_prestation['ouvert']){
            case 0 :
                $ouvert='non';
                break;
            case 1 :
                $ouvert='en cours';
                break;
            case 2 :
                $ouvert='totalement ouvert';
                break;
        }

        $utilise='';
        switch ($this->tableau_prestation['utilise']){
            case 0 :
                $utilise='non';
                break;
            case 1 :
                $utilise='oui';
                break;
        }


        $uri = \Slim\Slim::getInstance()->request->getRootUri();
        $string = "
            <div class=\"titre-panier\"> 
                <h3>L'état de votre coffret numéro ".$this->tableau_prestation['id']."</h3>
                <br>Votre url de gestion : 
                <br><a href='http://".$_SERVER['HTTP_HOST']."$uri/coffret/gestion?token=".$this->tableau_prestation['token_gestion']."'> http://".$_SERVER['HTTP_HOST']."$uri/coffret/gestion?token=".$this->tableau_prestation['token_gestion']." </a>
                <br>Votre url à offrir : 
                <br><a href='http://".$_SERVER['HTTP_HOST']."$uri/coffret/cadeau?token=".$this->tableau_prestation['token_cadeau']."'> http://".$_SERVER['HTTP_HOST']."$uri/coffret/cadeau?token=".$this->tableau_prestation['token_cadeau']."
                </a><br>
                <br> L'état de votre commande  : 
                <br> Transmis :".$tranmis."
                <br> Ouvert : ".$ouvert."
                <br> Utilisé : ".$utilise."
                <br>
                <br> Ce que contient votre commande :
                <br>
            ";

        foreach($this->validation as $prestation) {
        $string.= "
                <div class='col-sm-12 col-md-12'>
                    <div class='thumbnail'>
                        <img src=$uri/web/img/" . $prestation->img . " alt='$prestation->nom' id='img-catalogue'>
                        <div class='desc-detaillee'> 
                            <h3>" . $prestation->nom . "</h3>                             
                            Catégorie : " . $prestation->getCategorie()->nom . "
                            <br>Prix : " . $prestation->prix . " €
                            <br>
                            <br>" . $prestation->descr ."
                        </div>
                        ";

            $contient = null;
            foreach ($this->contient as $c) {
                if($c->idPrestation == $prestation->id) {
                    $contient = $c;
                }
            }
            $quantite = $contient->quantite;

            $string .="
                         <div class='desc-qte'> 
                             Quantité :" .$quantite."
                         </div>
                     </div>
                 </div>
            ";
        }


        return $string;

    }


    public function afficherFacture(){
        $slim = \Slim\Slim::getInstance();
        $string="
        <form method=\"post\" action=\"payer\">
         <p>
            <div class=\"validFact\">
                Nom :    <input type=\"text\" name=\"nom\" /><BR>
                Numéro de la carte : <input type=\"text\" name=\"numCB\" /><BR>
                Cryptogramme :   <input type=\"text\" name=\"crypto\" /><BR>
            </div>
            <BR>
            <a href=".$slim->urlFor('coffret/gestion').">
                  <div class='desc-bouton'>
                        <h3>Valider</h3>
                    </div>  
            </a>
            </p>
        </form>";
        return $string;
    }


    public function afficherCadeau(){

        $uri = \Slim\Slim::getInstance()->request->getRootUri();
        $route = \Slim\Slim::getInstance()->request->getResourceUri();

            $content ="            <div class=\"thumbnail\">
                            <img src=$uri/web/img/" . $this->tableau_prestation['img'] . " alt=" . $this->tableau_prestation['nom'] . " id='img-catalogue'>
                            <div class='desc-detaillee'> 
                                <h3>" . $this->tableau_prestation['nom'] . "</h3>
                                <br>
                                <br>" . $this->tableau_prestation['descr'] . "
                             <a href='$uri$route?token=".$_GET['token']."'>
                                <div class='desc-bouton'>
                                    <h5>Suivant</h5>
                                </div>
                            </a>
                            </div>";
            return $content;
    }

    public function afficherCadeaux() {
        $uri = \Slim\Slim::getInstance()->request->getRootUri();
        $route = \Slim\Slim::getInstance()->request->getResourceUri();

        $content = "
            <div class=\"titre-panier\">
               <h2> Vous pouvez remercier : ".$this->contient['prenom']."  ".$this->contient['nom']." pour ce magnifque cadeau en lui envoyant un email ! </h2>

               <h3>Voici son adresse : ".$this->contient['mail']."</h3>";
        if($this->contient['message']!=null){
            $content.="<h3>Il a un message pour vous : ".$this->contient['message']." <h3>";
        }

        $content.="<h3>Résumé de votre cadeau : </h3>
            </div>
        ";
        foreach($this->validation as $prestation) {
            $contient = null;
            foreach ($this->tableau_prestation as $c) {
                if ($c->idPrestation == $prestation->id) {
                    $contient = $c;
                }
            }
            $quantite = $contient->quantite;

            $content .= "
                    <div class=\"col-sm-12 col-md-12\">
                        <div class=\"thumbnail\">
                            <img src=$uri/web/img/" . $prestation->img . " alt='$prestation->nom' id='img-catalogue'>
                            <div class='desc-detaillee'> 
                                <h3>" . $prestation->nom . "</h3>
                                Catégorie : " . $prestation->getCategorie()->nom . "
                                <br>Prix : " . $prestation->prix . " €
                                <br>
                                <br>" . $prestation->descr . "
                            </div>
                            <div class='desc-qte'> 
                                Quantité : $quantite
                            </div>
                        </div>
                    </div>
                ";
        }

        return $content;
    }

    public function afficherAuth(){
        $slim = Slim::getInstance();
        $string = " <form method=\"post\" action=\"".$slim->urlFor("coffret/verif",['id' => $this->validation['id']])."\">
                        <p>Pour accéder a ce coffret il faut vous identifier !</p>
                        <input type=\"password\" name=\"motdepasse\" />
                        <div class=\"titre-panier\">
                            <input type=\"hidden\" name=\"_METHOD\" value=\"PUT\"/>
                            <input type=\"submit\" value=\"Valider\" />
                        <div/>
                    </form>";
        return $string;
    }


    public function mdpAccepte(){
        $slim= Slim::getInstance();
        $string = "<p>Mot de passe accepté, veuillez retrouver votre coffret ici :
                            <a href=".$slim->urlFor('coffret/gestion',['token'=>$this->validation['token_gestion']]).">
                                <div class='desc-bouton'>
                                    <h5>Gestion</h5>
                                </div>
                            </a>
                    </p>";
        return $string;
    }

    public function mdpRejete(){
        $slim = Slim::getInstance();
        $string = " <form method=\"post\" action=\"".$slim->urlFor("coffret/verif",['id' => $this->validation['id']])."\">
                       <p>Le mot de passe que vous avez donné est faux ! Veuillez réessayer :</p>
                        <input type=\"password\" name=\"motdepasse\" />
                        <div class=\"titre-panier\">
                            <input type=\"hidden\" name=\"_METHOD\" value=\"PUT\"/>
                            <input type=\"submit\" value=\"Valider\" />
                    <div/>
                    </form>";
        return $string;
    }

    public function render($selecteur)
    {
        $content = "";
        switch ($selecteur) {
            case 1 :
                $content .= $this->afficherListePrestations();
                break;
            case "ErreurValidation" :
                $content .= $this->afficherErreurValidation();
                break;
            case "Formulaire" :
                $content .= $this->afficherFormulaire();
                break;
            case "Recapitulatif" :
                $content .= $this->afficherRecapitulatif();
                break;
            case "Gestion" :
                $content .= $this->afficherPageGestion();
                break;
            case "Gestion_cagnotte" :
                $content .= $this->afficherPageGestionCagnotte();
                break;
            case "Cagnotte" :
                $content .= $this->afficherPageCagnotte();
                break;
            case "Facture" :
                $content .= $this->afficherFacture();
                break;
            case "Cadeau_ouverture":
                $content .=$this->afficherCadeau();
                break;
            case "Cadeau_recap":
                $content .=$this->afficherCadeaux();
                break;
            case"MotdePasse":
                $content .=$this->afficherAuth();
                break;
            case "BonMdp" :
                $content .=$this->mdpAccepte();
                break;
            case "MauvaisMdp" :
                $content .=$this->mdpRejete();
                break;
            }


        include 'entete.php';
        return "$HEADER $content $END";
    }
}