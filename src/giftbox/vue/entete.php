<?php
/**
 * Created by PhpStorm.
 * User: Guylan
 * Date: 17/12/2016
 * Time: 19:29
 */

$slim = \Slim\Slim::getInstance();


$uri = \Slim\Slim::getInstance()->request->getRootUri();


$head = "
    <head>
        <link href=\"$uri/web/img/giftbox2.png\" rel=\"shortcut icon\" >
        <title>GiftBox</title>
        <meta charset='UTF-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1.0'/>
        <link rel='stylesheet' href='$uri/web/css/bootstrap.css'>
        <link rel='stylesheet' href='$uri/web/css/moncss.css'>
    </head>
";


$navbar = "
    <div class=\"navbar navbar-inverse text-uppercase font-montserrat\" data-offset-top=\"197\" role=\"navigation\" >
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\"></button>
            <a class=\"navbar-brand\" href=" . $slim->urlFor('menu') . "><i class=\"fa fa-home\"></i><font size='2pt'><span class=\"glyphicon glyphicon-home\" aria-hidden=\"true\"></span></font> Menu</a>
        </div>
        <div class=\"navbar-collapse collapse\">
            <ul class=\"nav navbar-nav\">
                <li class=\"dropdown\">
                <a href='#' class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"fa fa-info\"></i> Catalogue<span class=\"caret\"></span></a>
                    <ul class=\"dropdown-menu\">
                        <li><a href=" . $slim->urlFor('catalogue/prestations') . ">Toutes les prestations</a></li>
                        <li role=\"separator\" class=\"divider\"></li>
                        <li><a href=" . $slim->urlFor('catalogue/categorie', ['id' => 1]) . ">Attention</a></li>
                        <li><a href=" . $slim->urlFor('catalogue/categorie', ['id' => 2]) . ">Activité</a></li>
                        <li><a href=" . $slim->urlFor('catalogue/categorie', ['id' => 3]) . ">Restauration</a></li>
                        <li><a href=" . $slim->urlFor('catalogue/categorie', ['id' => 4]) . ">Herbergement</a></li>
                    </ul>
                </li>
            </ul>
            <ul class=\"nav navbar-nav navbar-right\">
                <li>
                    <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\"></button>
                    <a class=\"navbar-brand\" href=" . $slim->urlFor('coffret') . "><i class=\"fa fa-home\"></i> <font size='2pt'><span class=\"glyphicon glyphicon-gift\" aria-hidden=\"true\"></span> Coffret</font></a>
                </li>
            </ul>
        </div>
    </div>
";


$header = "
    <header>
        $navbar
    </header>
";


$script = "
    <script src='$uri/web/js/jquery.min.js'></script>
    <script src='$uri/web/js/bootstrap.js'></script >
    <script src='$uri/web/js/monjs.js'></script >
";


$HEADER = "
    <!DOCTYPE html>
        <html lang='fr'>
            $head
            $header
            <body background='$uri/web/img/bg2.jpg'>
                <div class='container'>
";

$END = "
                </div>
                $script
            </body>
        <html>
";