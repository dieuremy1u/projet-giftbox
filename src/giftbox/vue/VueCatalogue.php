<?php
/**
 * Created by PhpStorm.
 * User: JEDETESTEMICROSOFT
 * Date: 14/12/2016
 * Time: 14:37
 */

namespace giftbox\vue;


class VueCatalogue
{
    private $prestations;

    /**
     * VueCatalogue constructor.
     * @param $presta prestations à traiter
     */
    public function __construct($presta)
    {
        $this->prestations = $presta;
    }


    /**
     * @return string html affichant les détail d'une prestation
     */
    private function afficherPrestation(){
        $slim = \Slim\Slim::getInstance();
        $uri = \Slim\Slim::getInstance()->request->getRootUri();
        $string = "
            <div class=\"thumbnail\">
                <img src=$uri/web/img/".$this->prestations['img']." alt='' id='img-catalogue-detail'>
                <div class='desc-detaillee'> 
                    <h3>" . $this->prestations["nom"] . "</h3>
                    Catégorie : ".$this->prestations->getCategorie()["nom"]."
                    <br>Prix : ".$this->prestations["prix"]." €
                    <br>
                    <br>".$this->prestations["descr"]."
                </div>
                <a href=".$slim->urlFor('coffret/ajout',['id' => $this->prestations["id"]]).">
                    <div class='desc-bouton'>
                        <h5>Ajouter au coffret</h5>
                    </div>
                </a>
                <div class='etoiles'>
                    ".$this->genererEtoiles($this->prestations)."
                    <div class='etoiles-text-detail'>
                        Nombre de notes : ".$this->prestations['nb_notes']."
                    </div>
                </div>
            </div>
        ";
        return $string;
    }


    /**
     * @return string html affichant la liste des prestations
     */
    private function afficherListePrestations()
    {
        $slim = \Slim\Slim::getInstance();
        $uri = \Slim\Slim::getInstance()->request->getRootUri();
        $route = \Slim\Slim::getInstance()->request->getResourceUri();
        $string = "
            <div class='bouton-tri'>
                <h4>Trier par prix : </h4> 
                <a href='$uri$route?tri=asc' class='btn btn-default' role='button' target='_self' >Croissant</a>
                <a href='$uri$route?tri=desc' class='btn btn-default' role='button' >Décroissant</a>
            </div>
        ";
        foreach($this->prestations as $prestation){
            $id = $prestation['id'];
            $string .= "
                <div class=\"col-sm-6 col-md-4\">
                    <div class=\"thumbnail\">
                        <img src=$uri/web/img/".$prestation['img']." alt='' id='img-catalogue'>
                        <a href=".$slim->urlFor('catalogue/prestation', ['id' => $id])." >
                            <div class='desc-bouton'>
                                <h3>" . $prestation["nom"] . "</h3>
                                Catégorie : ".$prestation->getCategorie()["nom"]."
                                <br>Prix : ".$prestation["prix"]." €
                            </div>
                        </a>
                        <a href=".$slim->urlFor('coffret/ajout',['id' => $id]).">
                            <div class='desc-bouton'>
                                <h5>Ajouter au coffret</h5>
                            </div>
                        </a>
                        <div class='etoiles'>
                            ".$this->genererEtoiles($prestation)."
                            <div class='etoiles-text'>
                                Nombre de notes : ".$prestation['nb_notes']."
                            </div>
                        </div>
                    </div>
                </div>
            ";
        }
        return $string;
    }


    /**
     * @param $prestation prestation pour laquelle on veut afficher les etoiles
     * @return string icones des etoiles
     */
    private function genererEtoiles($prestation) {
        $note = $prestation["note"];
        $id = $prestation["id"];
        $etoiles = "";
        for($i = 1; $i <= 5; $i++) { // numero de l'etoile de la prestation
            if($note >= $i)
                // etoiles pleines
                $class = "glyphicon glyphicon-star";
            else
                // etoiles vides
                $class = "glyphicon glyphicon-star-empty";

            // crée les étoiles
            if (isset($_COOKIE["vote_presta_$id"]) && intval($_COOKIE["vote_presta_$id"]) == $i)
                $etoiles .= "<span id='note-orange' class='$class' aria-hidden=\"true\"></span>";
            else if (isset($_COOKIE["vote_presta_$id"]))
                $etoiles .= "<span id='note-jaune' class='$class' aria-hidden=\"true\"></span>";
            else
                // crée un formulaire invisible prérempli pour envoyer les notes via une requete post
                $etoiles.= "<form action='' method=\"post\" id=\"id . $id . $i\" style=\"display:none\" >
                                <input type=\"hidden\" name='id-presta' value='$id' />
                                <input type=\"hidden\" name='note' value='$i' />
                            </form><a href='javascript:void(0)' onclick='setCookie(\"vote_presta_$id\", $i, 30, \" / \");insertNotation(\"id . $id . $i\")'><span id='$id.$i' class='$class' aria-hidden=\"true\"></span></a>";
        }
        return $etoiles;
    }


    /**
     * @param $selecteur mode d'affichage
     * @return string html correspondant à l'affichage de la page
     */
    public function render($selecteur) {
        $content = "";
        switch ($selecteur) {
            case 1 : {
                $content .= $this->afficherListePrestations();
                break;
            }
            case 2 : {
                $content .= $this->afficherPrestation();
                break;
            }
        }

        include 'entete.php';
        return "$HEADER $content $END";
    }
}