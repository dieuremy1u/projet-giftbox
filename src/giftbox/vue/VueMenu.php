<?php

/**
 * Created by PhpStorm.
 * User: Guylan
 * Date: 18/12/2016
 * Time: 13:00
 */

namespace giftbox\vue;

class VueMenu
{
    private $prestations;

    /**
     * VueCatalogue constructor.
     * @param $presta la meilleure prestation à afficher
     */
    public function __construct($prestations)
    {
        $this->prestations = $prestations;
    }


    /**
     * @param $prestation prestation pour laquelle on veut afficher les etoiles
     * @return string icones des etoiles
     */
    private function genererEtoiles($prestation) {
        $note = $prestation->note;
        $etoiles = "";
        for($i = 1; $i <= 5; $i++) { // numero de l'etoile de la prestation
            if($note >= $i)
                // etoiles pleines
                $class = "glyphicon glyphicon-star";
            else
                // etoiles vides
                $class = "glyphicon glyphicon-star-empty";

            // crée les étoiles
            $etoiles .= "<span id='note-jaune' class='$class' aria-hidden=\"true\"></span>";
        }
        return $etoiles;
    }


    /**
     * @return string la meilleure prestation
     */
    private function afficherCarousel() {
        $slim = \Slim\Slim::getInstance();
        $uri = \Slim\Slim::getInstance()->request->getRootUri();
        $html = " 
            <div id=\"carousel-menu\" class=\"carousel slide\" data-ride=\"carousel\">
                <div class=\"carousel-inner\" role=\"listbox\">
        ";
        $i = 0;
        foreach ($this->prestations as $prestation) { // diaporatives
            if($i == 0) {
                $html .= "
                    <div class=\"item active\">"; $i=1; }
            else
                $html .= "
                    <div class=\"item\">";
            $html .= "
                        <div id='thumbnail-carousel' class=\"thumbnail\" >
                            <a href=".$slim->urlFor('catalogue/categorie', ['id' => $prestation->getCategorie()->id]).">
                            <div class='desc-carousel'>
                                <h1>".$prestation->getCategorie()->nom."</h1>
                            </div>
                            </a> 
                            <img id='img-carousel' src=$uri/web/img/$prestation->img alt='$prestation->nom' >
                            <a href=".$slim->urlFor('catalogue/prestation', ['id' => $prestation->id]).">
                                <div class='desc-carousel'> 
                                    <h3>$prestation->nom</h3>
                                    Prix : $prestation->prix €
                                    <br>$prestation->descr
                                </div>
                            </a>
                            <div class='etoiles'>
                                ".$this->genererEtoiles($prestation)."
                            </div>  
                        </div>
                    </div>";
        }
        // controllers
        $html .= "  <a class=\"left carousel-control\" href=\"#carousel-menu\" role=\"button\" data-slide=\"prev\">
                        <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>
                        <span class=\"sr-only\">Précédent</span>
                    </a>
                    <a class=\"right carousel-control\" href=\"#carousel-menu\" role=\"button\" data-slide=\"next\">
                        <span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>
                        <span class=\"sr-only\">Suivant</span>
                    </a>
                </div>
            </div>";
        return $html;
    }

    /**
     * @return string textes du menu
     */
    private function afficherTextes() {
        $html = "
            <div class='col-sm-6 col-md-7'>
                <div id='thumbnail-texte' class=\"thumbnail\">
                    Bienvenue sur Giftbox ! Le site qui offre des boîtes qui déboîtes !
                    <br><br>Sur ce site vous trouverez une large quantité de cadeaux que vous pourrez offrir à vos proches et à vos amis !<br><br><br><br><br><br><br><br><br>
                </div>
            </div>
            <div class='col-sm-6 col-md-5'>
                <div id='thumbnail-texte' class=\"thumbnail\">
                    Les diverses offres sont divisées en 4 catégories :
                    <br><br>Les attentions : des petits cadeaux, qui montrent qu'on pense à l'autre.
                    <br><br>Les activitées : pour ceux qui recherchent de nouvelles expériences.
                    <br><br>La restauration : pour les plus gourmets.
                    <br><br>L'Hebergement : car il faut bien dormir de temps en temps.
                    <br><br><br>
                </div>
            </div>
            <div class='col-sm-12 col-md-12'>
                <div id='thumbnail-texte' class=\"thumbnail\">
                    Vous trouverez forcément votre bohneur (et surtout celui d'un proche) dans notre catalogue.
                    <br><br>Vous pouvez aussi trouver nos produits les mieux notés dans chaque catégorie juste ci-dessus.
                    <br><br>Une fois satisfait de vos achats vous pourrez valider votre coffret en cliquant sur l'onglet \"coffret\" en haut à droite de votre écran.<br><br><br>
                </div>
            </div>
        ";
        return $html;
    }


    /**
     * @param $selecteur mode d'affichage
     * @return string html correspondant à l'affichage de la page
     */
    public function render($selecteur) {
        $content = "";
        switch ($selecteur) {
            case 1 : {
                $content .= $this->afficherCarousel();
                $content .= $this->afficherTextes();
                break;
            }
        }

        include 'entete.php';
        return "$HEADER $content $END";
    }
}