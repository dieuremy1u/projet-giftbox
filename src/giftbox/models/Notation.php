<?php
/**
 * Created by PhpStorm.
 * User: Guylan
 * Date: 19/12/2016
 * Time: 13:48
 */

namespace giftbox\models;
use Illuminate\Database\Eloquent\Model;

class Notation extends Model
{
    protected $table = 'notation';
    protected $primaryKey='id';
    public $timestamps=false;

    public function getPrestation()
    {
        return Prestation::find($this->presta_id) ;
    }
}