<?php

/**
 * Created by PhpStorm.
 * User: JEDETESTEMICROSOFT
 * Date: 06/12/2016
 * Time: 11:29
 */
namespace giftbox\models;
use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    protected $table = 'categorie';
    protected $primaryKey='id';
    public $timestamps=false;

}