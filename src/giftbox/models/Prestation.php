<?php
/**
 * Created by PhpStorm.
 * User: JEDETESTEMICROSOFT
 * Date: 06/12/2016
 * Time: 11:32
 */

namespace giftbox\models;
use Illuminate\Database\Eloquent\Model;

class Prestation extends Model
{
    protected $table = 'prestation';
    protected $primaryKey='id';
    public $timestamps=false;

    public function getCategorie()
    {
        return Categorie::find($this->cat_id) ;
    }


}