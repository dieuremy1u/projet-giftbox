<?php
/**
 * Created by PhpStorm.
 * User: Guylan
 * Date: 18/12/2016
 * Time: 12:59
 */

namespace giftbox\controler;

use \giftbox\models\Prestation;
use \giftbox\models\Categorie;
use \giftbox\vue\VueMenu;

class ControlerMenu
{
    public function afficherMenu() {
        $categories = Categorie::get();
        foreach ($categories as $categorie)
            $prestations[] = Prestation::where('cat_id', $categorie->id)->orderBy('note', 'DESC')->first();

        $vue = new VueMenu($prestations);
        echo $vue->render(1);
    }
}