<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 14/12/2016
 * Time: 17:00
 */

namespace giftbox\controler;

use \giftbox\models\Prestation;
use \giftbox\models\Categorie;
use \giftbox\models\Notation;
use \giftbox\vue\VueCatalogue;

class ControlerCatalogue
{
    /**
     * ajoute la note transmise par le formulaire dans la base de données
     */
    public function insertNote() {
        // insere la note dans notation (et update les prestations grace au trigger)
        $note = new Notation();
        $note->presta_id = $_POST['id-presta'];
        $note->note = $_POST['note'];
        $note->save();

        // redirige la requete pour eviter le renvoie du formulaire
        header('Location: ' . \Slim\Slim::getInstance()->request->getContentCharset());
        exit;
    }


    /**
     * affiche une liste de categorie
     * @param int $cat categorie pour laquelle afficher la liste de prestations, -1 par défaut pour afficher toutes les préstations du catalogue
     */
    public function afficherPrestations($cat = -1) {
        // prestations pour une categorie donnee
        if($cat >= 0) {
            $categorie = Categorie::where('id', $cat)->first();
            if(isset($_GET["tri"])) {
                if(strcasecmp($_GET["tri"], "desc") == 0)
                    $prestations = Prestation::where('cat_id', $categorie->id)->orderBy('prix', 'DESC')->get();
                else if(strcasecmp($_GET["tri"], "asc") == 0)
                    $prestations = Prestation::where('cat_id', $categorie->id)->orderBy('prix', 'ASC')->get();
                else
                    $prestations = Prestation::where('cat_id', $categorie->id)->get();
            }
            else
                $prestations = Prestation::where('cat_id', $categorie->id)->get();
        }
        // toutes les prestations
        else {
            if (isset($_GET["tri"])) {
                if (strcasecmp($_GET["tri"], "desc") == 0)
                    $prestations = Prestation::orderBy('prix', 'DESC')->get();
                else if (strcasecmp($_GET["tri"], "asc") == 0)
                    $prestations = Prestation::orderBy('prix', 'ASC')->get();
                else
                    $prestations = Prestation::get();
            } else
                $prestations = Prestation::get();
        }

        $vue = new VueCatalogue($prestations);
        echo $vue->render(1);
    }


    /**
     * affiche le détail d'une prestation donnée
     * @param $id id de la prestation a afficher
     */
    public function afficherPrestationDetaillee($id) {
        $prestation = Prestation::where('id', $id)->first();
        $vue = new VueCatalogue($prestation);
        echo $vue->render(2);
    }
}