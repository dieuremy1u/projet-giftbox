<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 14/12/2016
 * Time: 17:01
 */

namespace giftbox\controler;


use giftbox\models\Categorie;
use giftbox\models\Coffret;
use giftbox\models\Contient;
use giftbox\models\Prestation;
use giftbox\vue\VueCoffret;
use RandomLib\Factory;

class ControlerCoffret
{

    private function genererToken() {
        $factory = new Factory();
        $generator = $factory->getMediumStrengthGenerator();
        $token = $generator->generateString(26, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789');
        return $token;
    }

    public function ajoutPrestation($id){

        if(isset($_SESSION['prestations_coffret'][$id])){
            $_SESSION['prestations_coffret'][$id]+=1;
        }else {
            $_SESSION['prestations_coffret'][$id] = 1;
        }
        // redirige la requete pour afficher le coffret
        header('Location: ' . \Slim\Slim::getInstance()->request->getRootUri() . '/coffret');
        exit;
    }

    public function supprimerPrestation($id){
        if(isset($_SESSION['prestations_coffret'][$id])){
            $_SESSION['prestations_coffret'][$id]-=1;
            if($_SESSION['prestations_coffret'][$id]<=0){
                unset($_SESSION['prestations_coffret'][$id]);
            }
        }
        header('Location: ' . \Slim\Slim::getInstance()->request->getRootUri() . '/coffret');
        exit;

    }
    public function afficherContenuCoffret() {
        $tab=array();
        if(isset($_SESSION['prestations_coffret'])){
            foreach(array_keys($_SESSION['prestations_coffret']) as $id){
                $tab[$id]=Prestation::where('id',$id)->first();
            }
        }
        $vue = new VueCoffret($tab);
        echo $vue->render(1);
    }

    public function validerCoffret(){
        //On récupère les infos de session
        $tab=array();
        $CoffretValide=false;

        if(isset($_SESSION['prestations_coffret'])){
            foreach(array_keys($_SESSION['prestations_coffret']) as $id){
                $tab[$id]=Prestation::where('id',$id)->first();
            }


        //Teste si il y'a au moins 2 Prestations de 2 Catégories différentes
            if(sizeof($tab)>1){
                $Categorie1=end($tab)->getCategorie()->nom;
                while(($Categorie2=prev($tab))!=false){
                    if($Categorie1!=$Categorie2->getCategorie()->nom){
                        $CoffretValide=true;
                        break;
                    }
                }
            }
        }
        $vue = new VueCoffret();
        if($CoffretValide){
            //Renvoi un formulaire pour valider le coffret.
            echo $vue->render("Formulaire");
        }else{
            //Renvoi un message d'erreur.
            echo $vue->render("ErreurValidation");
        }
    }

    public function verificationMotDePasse($id){
        if(password_verify($_POST['motdepasse'],Coffret::where('id',$id)->first()->hash)){
            $_SESSION['auth']=$id;
            header('Location: ' . \Slim\Slim::getInstance()->request->getRootUri() . '/coffret/gestion?token=' . Coffret::where('id',$id)->first()->token_gestion);
            exit;
        }else{
            $vue = new VueCoffret(null,Coffret::where('id',$id)->first());
            echo $vue->render('MauvaisMdp');
        }
    }

    public function gererCadeau(){
        if(isset($_GET['token'])) {
            $coffret = Coffret::where('token_gestion', '=', $_GET['token'])->first();
            if ($coffret != null) {
                if(isset($_POST['ajout'])){
                    $coffret->total_paye+=$_POST['ajout'];
                    $coffret->save();
                }

                if ($coffret['mdp']=='oui' && (!isset($_SESSION['auth']) || $_SESSION['auth'] != $coffret['id'])) {
                    $vue = new VueCoffret(null,$coffret);
                    echo $vue->render("MotdePasse");
                } else {
                    $contient = Contient::where('idCoffret', '=', $coffret['id'])->get();

                    $prestations = "";
                    foreach ($contient as $c) {
                        $prestations[] = Prestation::where('id', '=', $c['idPrestation'])->first();
                    }

                    $vue = new VueCoffret($coffret, $prestations, $contient);

                    if ($coffret->paiement == 'classique' || $coffret->prix <= $coffret->total_paye) {
                        echo $vue->render("Gestion");
                    }
                    else if ($coffret->paiement == 'cagnotte') {
                        echo $vue->render("Gestion_cagnotte");
                    }
                }
            }
        }
    }

    public function afficherCagnote(){
        if(isset($_GET['token'])) {
            $coffret = Coffret::where('token_cagnotte', '=', $_GET['token'])->first();
            if ($coffret != null) {
                if (isset($_POST['ajout'])) {
                    $coffret->total_paye += $_POST['ajout'];
                    $coffret->save();
                }
                $contient = Contient::where('idCoffret', '=', $coffret['id'])->get();

                $prestations = "";
                foreach ($contient as $c) {
                    $prestations[] = Prestation::where('id', '=', $c['idPrestation'])->first();
                }
                $vue = new VueCoffret($coffret, $prestations, $contient);
                echo $vue->render("Cagnotte");
            }
        }
    }

    public function afficherPrestationCadeau(){
        if(isset($_GET['token'])){
            $coffret=Coffret::where('token_cadeau','=', $_GET['token'])->first();
            if($coffret!=null){
                $coffret->transmis=1;
                $coffret->ouvert=1;
                $coffret->save();
                $contient = Contient::where('idCoffret','=', $coffret['id'])
                    ->where('affiche','=', 0)
                    ->first();
                if($contient!=null) {
                    $prestation = Prestation::where('id', '=', $contient['idPrestation'])->first();
                    $vue = new VueCoffret($prestation);
                    echo $vue->render("Cadeau_ouverture");
                    $contient->affiche = 1;
                    $contient->save();
                }
                else {
                    $contients = Contient::where('idCoffret','=', $coffret['id'])->get();
                    $prestations = "";
                    foreach ($contients as $c) {
                        $prestations[] = Prestation::where('id','=', $c['idPrestation'])->first();
                    }
                    $coffret->ouvert=2;
                    $coffret->save();
                    $vue = new VueCoffret($contients, $prestations,$coffret);
                    echo $vue->render("Cadeau_recap");
                }
            }
        }

    }

    public function enregistrerCoffret(){

        $mail = $_SESSION['valid']['mail'];
        $prenom = $_SESSION['valid']['prenom'];
        $nom = $_SESSION['valid']['nom'];
        $message = $_SESSION['valid']['message'];
        $paiement = $_SESSION['valid']['paiement'];
        if($_SESSION['valid']['mdp']==='oui') {
            $hash = $_SESSION['valid']['hash'];
            //Cree un coffret dans la BDD[Coffret] avec les info du post
            $coffret = Coffret::create(['nom' => $nom,
                'prenom' => $prenom,
                'mail' => $mail,
                'message' => $message,
                'paiement' => $paiement,
                'prix' => $_SESSION['cout'],
                'mdp'=> 'oui',
                'hash' => $hash]); // a faire
        }else{
            $coffret = Coffret::create(['nom'=>$nom,
                'prenom'=>$prenom,
                'mail'=>$mail,
                'message'=>$message,
                'paiement'=>$paiement,
                'mdp'=>'non',
                'prix' => $_SESSION['cout']]);
        }

        unset($_SESSION['valid']);

        $_SESSION['coffret_actuel']=Coffret::where('id',$coffret['id'])->first();

        //Pour chaque prestation dans la session, on l'ajoute a la BDD[Contient] avec sa quantite
        foreach(array_keys($_SESSION['prestations_coffret']) as $id) {
            $cont = new Contient;
            $cont->idCoffret=$coffret['id'];
            $cont->idPrestation=$id;
            $cont->quantite=$_SESSION['prestations_coffret'][$id];
            $cont->save();
        }

        //On passe l'objet coffret a la vue.
        $vue=new VueCoffret(null,$coffret);
        header('Location: ' . \Slim\Slim::getInstance()->request->getRootUri() . '/coffret/payer');
        exit;
    }

    public function recapCoffret(){
        $_SESSION['valid']['mail'] = $_POST['mail'];
        $_SESSION['valid']['prenom'] = $_POST['prenom'];
        $_SESSION['valid']['nom'] = $_POST['nom'] ;
        $_SESSION['valid']['message'] = $_POST['message'];
        $_SESSION['valid']['paiement'] = $_POST['paiement'];
        $_SESSION['valid']['mdp']= $_POST['mdp'];
        $_SESSION['valid']['hash']=password_hash($_POST['motdepasse'],PASSWORD_DEFAULT);

        $tab=array();
        if(isset($_SESSION['prestations_coffret'])){
            foreach(array_keys($_SESSION['prestations_coffret']) as $id){
                $tab[$id]=Prestation::where('id',$id)->first();
            }
        }
        $vue = new VueCoffret($tab,$_SESSION['valid']);
        echo $vue->render("Recapitulatif");
    }

    public function genererUrl($nompage, $token){
        $uri = \Slim\Slim::getInstance()->request->getRootUri();

        switch ($nompage) {
            case "gestion":
                $url = $_SERVER['HTTP_HOST']."$uri/coffret/gestion?token=$token";
                break;
            case "cadeau":
                $url = $_SERVER['HTTP_HOST']."/$uri/coffret/cadeau?token=$token";
                break;
            case "cagnote":
                $url = $_SERVER['HTTP_HOST']."/$uri/coffret/cagnote?token=$token";
                break;
            default:
                $url = "";
        }
        return $url;
    }

    public function paiement()
    {
        if(isset($_SESSION['coffret_actuel'])) {
            $cof = $_SESSION['coffret_actuel'];
            $pai = $cof['paiement'];
            if (isset($_SESSION['prestations_coffret']) && $pai === 'classique') {
                $vue = new VueCoffret();
                echo $vue->render("Facture");
            }
            if (isset($_SESSION['prestations_coffret']) && $pai === 'cagnotte') {
                $vue = new VueCoffret();
                echo $vue->render("Facture");
            }
        }
    }

    public function validerPaiement(){
        if (isset($_SESSION['prestations_coffret'])){
            $tokengestion = $this->genererToken();
            $tokencadeau = $this->genererToken();
            $cof=$_SESSION['coffret_actuel'];

            $idcof= $cof['id'];
            $coffret=Coffret::where('id',$idcof)->first();
            $coffret->est_paye=1;
            $coffret->token_gestion=$tokengestion;
            $coffret->token_cadeau=$tokencadeau;
            if($cof['paiement']==='cagnotte'){
                $tokencagnotte = $this->genererToken();
                $coffret->token_cagnotte=$tokencagnotte;
            }
            $coffret->save();
            unset($_SESSION['prestations_coffret']);
            header('Location: ' . \Slim\Slim::getInstance()->request->getRootUri() . '/coffret/gestion?token=' . $tokengestion);
            exit;
        }
    }
}