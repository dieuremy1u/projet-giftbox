/**
 * Created by Guylan on 20/12/2016.
 */


function setCookie(nom, valeur, expire, chemin)
{
    var expDate = new Date();
    expDate.setTime(expDate.getTime() + (expire * 86400000));
    document.cookie = nom + "=" + encodeURIComponent(valeur) + ";expires=" + expDate.toGMTString() + ";path =" + chemin;
}

function insertNotation($id)
{
    document.getElementById($id).submit();
}