-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 17 Janvier 2017 à 22:53
-- Version du serveur :  10.1.16-MariaDB
-- Version de PHP :  5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `giftbox`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `id` int(11) NOT NULL,
  `nom` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `categorie`
--

INSERT INTO `categorie` (`id`, `nom`) VALUES
(1, 'Attention'),
(2, 'Activité'),
(3, 'Restauration'),
(4, 'Hébergement');

-- --------------------------------------------------------

--
-- Structure de la table `coffret`
--

CREATE TABLE `coffret` (
  `id` int(11) NOT NULL,
  `nom` varchar(20) NOT NULL,
  `prenom` varchar(20) NOT NULL,
  `mail` varchar(25) NOT NULL,
  `message` text,
  `paiement` varchar(10) NOT NULL,
  `est_paye` tinyint(1) NOT NULL DEFAULT '0',
  `token_gestion` varchar(26) DEFAULT NULL,
  `token_cadeau` varchar(26) DEFAULT NULL,
  `transmis` tinyint(1) NOT NULL DEFAULT '0',
  `ouvert` int(1) NOT NULL DEFAULT '0',
  `utilise` tinyint(1) NOT NULL DEFAULT '0',
  `prix` decimal(11,2) NOT NULL,
  `total_paye` decimal(11,2) NOT NULL DEFAULT '0.00',
  `hash` varchar(256) DEFAULT NULL,
  `mdp` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `coffret`
--

INSERT INTO `coffret` (`id`, `nom`, `prenom`, `mail`, `message`, `paiement`, `est_paye`, `token_gestion`, `token_cadeau`, `transmis`, `ouvert`, `utilise`, `prix`, `total_paye`, `hash`, `mdp`) VALUES
(35, 'Desaint-Acheul', 'Aymon', 'noxyam404@gmail.com', 'J''espère que ce cadeau te plaira !', 'cagnotte', 1, 'fngpVAvQVstiA7YjF5i2IaQbOV', 'Wx5PZ8QfqjwEEvvXgIFqyEZyRf', 0, 0, 0, '194.00', '194.00', '$2y$10$xMLq88tkPcgHh0yOExZMweA40s1Ux0R099y54/.M2g2cDvTSUodO.', 'oui'),
(36, 'Desaint-Acheul', 'Aymon', 'noxyam404@gmail.com', 'J''espère que ce cadeau te plaira !', 'cagnotte', 1, 'f4OXsZKIH4Cat2uQflW6olcPXS', '5h8m1XgN1Jh9fJemtv1vbwHvYy', 0, 0, 0, '70.00', '70.00', '$2y$10$E4xAaEHziCeP18rYd2AvluCdpTkrHwqhLbLF3.tjxxnvIIyMvaMbS', 'oui');

-- --------------------------------------------------------

--
-- Structure de la table `contient`
--

CREATE TABLE `contient` (
  `idCoffret` int(5) NOT NULL,
  `idPrestation` int(5) NOT NULL,
  `quantite` int(5) NOT NULL,
  `affiche` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `contient`
--

INSERT INTO `contient` (`idCoffret`, `idPrestation`, `quantite`, `affiche`) VALUES
(35, 2, 1, 0),
(35, 24, 1, 0),
(36, 5, 1, 0),
(36, 7, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `notation`
--

CREATE TABLE `notation` (
  `id` int(11) NOT NULL,
  `presta_id` int(11) NOT NULL,
  `note` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `notation`
--

INSERT INTO `notation` (`id`, `presta_id`, `note`) VALUES
(3, 3, 2),
(4, 2, 4),
(5, 7, 5),
(6, 24, 3);

--
-- Déclencheurs `notation`
--
DELIMITER $$
CREATE TRIGGER `delete_note_notation` AFTER DELETE ON `notation` FOR EACH ROW BEGIN
    DECLARE v decimal(5,2);
    DECLARE nb int;
   
    SELECT AVG(notation.note) INTO v
    FROM prestation INNER JOIN notation ON prestation.id = notation.presta_id
    WHERE prestation.id = OLD.presta_id;
    
    SELECT prestation.nb_notes INTO nb
    FROM prestation
    WHERE prestation.id = OLD.presta_id;

    UPDATE prestation
    SET prestation.note = v
    WHERE prestation.id = OLD.presta_id;
    
    UPDATE prestation
    SET prestation.nb_notes = nb-1
    WHERE prestation.id = OLD.presta_id;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `insert_note_notation` AFTER INSERT ON `notation` FOR EACH ROW BEGIN
    DECLARE v decimal(5,2);
    DECLARE nb int;
   
    SELECT AVG(notation.note) INTO v
    FROM prestation INNER JOIN notation ON prestation.id = notation.presta_id
    WHERE prestation.id = NEW.presta_id;
    
    SELECT prestation.nb_notes INTO nb
    FROM prestation
    WHERE prestation.id = NEW.presta_id;

    UPDATE prestation
    SET prestation.note = v
    WHERE prestation.id = NEW.presta_id;
    
    UPDATE prestation
    SET prestation.nb_notes = nb+1
    WHERE prestation.id = NEW.presta_id;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_note_notation` AFTER UPDATE ON `notation` FOR EACH ROW BEGIN
    DECLARE v decimal(5,2);
   
    SELECT AVG(notation.note) INTO v
    FROM prestation INNER JOIN notation ON prestation.id = notation.presta_id
    WHERE prestation.id = NEW.presta_id;

    UPDATE prestation
    SET prestation.note = v
    WHERE prestation.id = NEW.presta_id;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `prestation`
--

CREATE TABLE `prestation` (
  `id` int(11) NOT NULL,
  `nom` text NOT NULL,
  `descr` text NOT NULL,
  `cat_id` int(11) NOT NULL,
  `img` text NOT NULL,
  `prix` decimal(5,2) NOT NULL,
  `note` decimal(5,2) DEFAULT '0.00',
  `nb_notes` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `prestation`
--

INSERT INTO `prestation` (`id`, `nom`, `descr`, `cat_id`, `img`, `prix`, `note`, `nb_notes`) VALUES
(1, 'Champagne', 'Bouteille de champagne + flutes + jeux à gratter', 1, 'champagne.jpg', '20.00', NULL, 0),
(2, 'Musique', 'Partitions de piano à 4 mains', 1, 'musique.jpg', '25.00', '4.00', 1),
(3, 'Exposition', 'Visite guidée de l’exposition ‘REGARDER’ à la galerie Poirel', 2, 'poirelregarder.jpg', '14.00', '2.00', 1),
(4, 'Goûter', 'Goûter au FIFNL', 3, 'gouter.jpg', '20.00', '0.00', 0),
(5, 'Projection', 'Projection courts-métrages au FIFNL', 2, 'film.jpg', '10.00', '0.00', 0),
(6, 'Bouquet', 'Bouquet de roses et Mots de Marion Renaud', 1, 'rose.jpg', '16.00', '0.00', 0),
(7, 'Diner Stanislas', 'Diner à La Table du Bon Roi Stanislas (Apéritif /Entrée / Plat / Vin / Dessert / Café / Digestif)', 3, 'bonroi.jpg', '60.00', '5.00', 1),
(8, 'Origami', 'Baguettes magiques en Origami en buvant un thé', 3, 'origami.jpg', '12.00', '0.00', 0),
(9, 'Livres', 'Livre bricolage avec petits-enfants + Roman', 1, 'bricolage.jpg', '24.00', '0.00', 0),
(10, 'Diner  Grand Rue ', 'Diner au Grand’Ru(e) (Apéritif / Entrée / Plat / Vin / Dessert / Café)', 3, 'grandrue.jpg', '59.00', '0.00', 0),
(11, 'Visite guidée', 'Visite guidée personnalisée de Saint-Epvre jusqu’à Stanislas', 2, 'place.jpg', '11.00', '0.00', 0),
(12, 'Bijoux', 'Bijoux de manteau + Sous-verre pochette de disque + Lait après-soleil', 1, 'bijoux.jpg', '29.00', '0.00', 0),
(13, 'Opéra', 'Concert commenté à l’Opéra', 2, 'opera.jpg', '15.00', '0.00', 0),
(14, 'Thé Hotel de la reine', 'Thé de debriefing au bar de l’Hotel de la reine', 3, 'hotelreine.gif', '5.00', '0.00', 0),
(15, 'Jeu connaissance', 'Jeu pour faire connaissance', 2, 'connaissance.jpg', '6.00', '0.00', 0),
(16, 'Diner', 'Diner (Apéritif / Plat / Vin / Dessert / Café)', 3, 'diner.jpg', '40.00', NULL, 0),
(17, 'Cadeaux individuels', 'Cadeaux individuels sur le thème de la soirée', 1, 'cadeaux.jpg', '13.00', '0.00', 0),
(18, 'Animation', 'Activité animée par un intervenant extérieur', 2, 'animateur.jpg', '9.00', '0.00', 0),
(19, 'Jeu contacts', 'Jeu pour échange de contacts', 2, 'contact.png', '5.00', '0.00', 0),
(20, 'Cocktail', 'Cocktail de fin de soirée', 3, 'cocktail.jpg', '12.00', '0.00', 0),
(21, 'Star Wars', 'Star Wars - Le Réveil de la Force. Séance cinéma 3D', 2, 'starwars.jpg', '12.00', '0.00', 0),
(22, 'Concert', 'Un concert à Nancy', 2, 'concert.jpg', '17.00', '0.00', 0),
(23, 'Appart Hotel', 'Appart’hôtel Coeur de Ville, en plein centre-ville', 4, 'apparthotel.jpg', '56.00', '0.00', 0),
(24, 'Hôtel d''Haussonville', 'Hôtel d''Haussonville, au coeur de la Vieille ville à deux pas de la place Stanislas', 4, 'hotel_haussonville_logo.jpg', '169.00', '3.00', 1),
(25, 'Boite de nuit', 'Discothèque, Boîte tendance avec des soirées à thème & DJ invités', 2, 'boitedenuit.jpg', '32.00', '0.00', 0),
(26, 'Planètes Laser', 'Laser game : Gilet électronique et pistolet laser comme matériel, vous voilà équipé.', 2, 'laser.jpg', '15.00', '0.00', 0),
(27, 'Fort Aventure', 'Découvrez Fort Aventure à Bainville-sur-Madon, un site Accropierre unique en Lorraine ! Des Parcours Acrobatiques pour petits et grands, Jeu Mission Aventure, Crypte de Crapahute, Tyrolienne, Saut à l''élastique inversé, Toboggan géant... et bien plus encore.', 2, 'fort.jpg', '25.00', '0.00', 0);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `coffret`
--
ALTER TABLE `coffret`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `contient`
--
ALTER TABLE `contient`
  ADD PRIMARY KEY (`idCoffret`,`idPrestation`),
  ADD KEY `frn_key_idPrestation` (`idPrestation`);

--
-- Index pour la table `notation`
--
ALTER TABLE `notation`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `prestation`
--
ALTER TABLE `prestation`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `coffret`
--
ALTER TABLE `coffret`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT pour la table `notation`
--
ALTER TABLE `notation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `prestation`
--
ALTER TABLE `prestation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `contient`
--
ALTER TABLE `contient`
  ADD CONSTRAINT `frn_key_idCoffret` FOREIGN KEY (`idCoffret`) REFERENCES `coffret` (`id`),
  ADD CONSTRAINT `frn_key_idPrestation` FOREIGN KEY (`idPrestation`) REFERENCES `prestation` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
