<?php
/**
 * Created by PhpStorm.
 * User: Aymon DesaintAcheul
 * Date: 06/12/2016
 * Time: 11:40
 */

include 'vendor/autoload.php';
use \conf\Eloquent;
use \giftbox\controler\ControlerCatalogue;
use \giftbox\controler\ControlerCoffret;
use \giftbox\controler\ControlerMenu;

session_start();
Eloquent::init('src\conf\conf.ini');
$app = new \Slim\Slim();



$app->get('/', function(){
    $controleur = new ControlerMenu();
    $controleur->afficherMenu();
})->name("menu");



$app->get('/catalogue/prestations', function(){
    $controleur = new ControlerCatalogue();
    $controleur->afficherPrestations();
})->name("catalogue/prestations");

$app->post('/catalogue/prestations', function(){
    $controleur = new ControlerCatalogue();
    $controleur->insertNote();
});



$app->get('/catalogue/prestation/:id', function($id){
    $controleur = new ControlerCatalogue();
    $controleur->afficherPrestationDetaillee($id);
})->name("catalogue/prestation");

$app->post('/catalogue/prestation/:id', function(){
    $controleur = new ControlerCatalogue();
    $controleur->insertNote();
});



$app->get('/catalogue/categorie/:id', function($id){
    $controleur = new ControlerCatalogue();
    $controleur->afficherPrestations($id);
})->name("catalogue/categorie");

$app->post('/catalogue/categorie/:id', function(){
    $controleur = new ControlerCatalogue();
    $controleur->insertNote();
});



$app->get('/coffret', function(){
    $controleur = new ControlerCoffret();
    $controleur->afficherContenuCoffret();
})->name("coffret");


$app->get('/coffret/ajout/:id', function($id){
    $controleur = new ControlerCoffret();
    $controleur->ajoutPrestation($id);
})->name('coffret/ajout');

$app->get('/coffret/gestion',function (){
    $controleur=new ControlerCoffret();
    $controleur->validerPaiement();
    $controleur->gererCadeau();
})->name('coffret/gestion');

$app->put('/coffret/gestion2',function (){
    $controleur=new ControlerCoffret();
    $controleur->gererCadeau();
})->name('coffret/gestion2');

$app->get('/coffret/cagnote',function (){
    $controleur=new ControlerCoffret();
    $controleur->afficherCagnote();
})->name('coffret/cagnote');

$app->put('/coffret/cagnote2',function (){
    $controleur=new ControlerCoffret();
    $controleur->afficherCagnote();
})->name('coffret/cagnote2');


$app->get('/coffret/supp/:id', function($id){
    $controleur = new ControlerCoffret();
    $controleur->supprimerPrestation($id);
})->name('coffret/supp');


$app->get('/coffret/cadeau',function(){
    $controleur = new ControlerCoffret();
    $controleur->afficherPrestationCadeau();
})->name('coffret/cadeau');


$app->get('/coffret/recap', function(){
    $controleur = new ControlerCoffret();
    $controleur->recapCoffret();
})->name("coffret/recap");

$app->get('/coffret/enregistrer', function(){
    $controleur = new ControlerCoffret();
    $controleur->enregistrerCoffret();
})->name("coffret/enregistrer");

$app->get('/coffret/valider', function(){
    $controleur = new ControlerCoffret();
    $controleur->validerCoffret();
})->name("coffret/valider");


$app->get('/coffret/urlcadeau', function(){
    $controleur = new ControlerCoffret();
    $controleur->offrirUnCadeau();
})->name("coffret/urlcadeau");


$app->put('/coffret/recap', function(){
    $controleur = new ControlerCoffret();
    $controleur->recapCoffret();
});

$app->get('/coffret/payer', function(){
    $controleur = new ControlerCoffret();
    $controleur->paiement();
})->name("coffret/payer");


$app->put('/coffret/verif/:id', function($id){
    $controleur = new ControlerCoffret();
    $controleur->verificationMotDePasse($id);
})->name("coffret/verif");



$app->run();